package com.softserve.edu;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class HelpingUtils {

    private static final int MAX = 999_999;

    public static Algorithms determineAlgorithm(String filename) throws IOException {

        String s = Files.readAllLines(Paths.get(filename), StandardCharsets.UTF_8).get(0);
        if(s.equals("Moscow")) {
            return Algorithms.Moscow;
        } else if(s.equals("Piter")) {
            return Algorithms.Piter;
        } else {
            return Algorithms.None;
        }

    }

    public static int countHappyTickets(Algorithms alg) {

        int counter  = 0;
        for(int i = 0; i <= MAX; i++) {
            if(isHappyTicket(i, alg)) counter++;
        }
        return counter;
    }

    private static boolean isHappyTicket(int number, Algorithms algorithm) {

        if( number < 0 || number > MAX ) {
            throw new IllegalArgumentException("Ticket number must be 6 digit decimal number 0.." + MAX);
        }

        switch(algorithm) {

            case Moscow:
                return moscowAlg(number);
            case Piter:
                return piterAlg(number);
            case None:
                throw new IllegalArgumentException("Undefined algorithm");
        }
        return true;
    }

    private static boolean moscowAlg(int n) {

        if(n == 0) return true;

        if(n > 0 && n <= 1000 ) {

            return false; // always unlucky for this range

        }

        String s = Integer.valueOf(n).toString();
        char[] chars = s.toCharArray();

        int firstSumm = 0;
        int secondSumm = 0;

        // Calculate sum of the first 3 numbers
        if(n >= 1001 && n <= 9_999) {
            firstSumm = Character.getNumericValue(chars[0]); // translate char to int
        } else if( n >= 10_000 && n <= 99_000) {
            firstSumm = Character.getNumericValue(chars[0]) + Character.getNumericValue(chars[1]);
        } else {
            for(int i = 0; i < 3; i++) {
                firstSumm += Integer.parseInt(Character.valueOf(chars[i]).toString());
            }
        }

        // second sum
        secondSumm = Character.getNumericValue(chars[chars.length - 1]) +
                      Character.getNumericValue(chars[chars.length - 2]) +
                      Character.getNumericValue(chars[chars.length - 3]);


        // if these sums are equal, ticket is considered happy
        return firstSumm == secondSumm;

    }

    private static boolean piterAlg(int number) {

        int evenSum = 0;
        int oddSum = 0;

        char[] chars = Integer.valueOf(number).toString().toCharArray();

        for(int i = 0; i < chars.length; i++) {
            int n = Character.getNumericValue(chars[i]);
            if(n % 2 == 0) {
                evenSum += n;
            } else {
                oddSum += n;
            }
        }
        return evenSum == oddSum;
    }

}
