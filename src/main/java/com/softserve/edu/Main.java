package com.softserve.edu;

import java.io.IOException;

public class Main {

    public static void main(String[] args) {

        switch (args.length) {
            case 0:
                typeInstructions();
                break;
            default:
                String filename = args[0];
                try {
                    Algorithms algorithm = HelpingUtils.determineAlgorithm(filename);
                    int result = HelpingUtils.countHappyTickets(algorithm);
                    System.out.println("According to " + algorithm + " the number of happy tickets is " + result);
                } catch (IOException e) {
                    System.out.println("IO Exception occurred. Details:");
                    System.out.println(e.getMessage());
                }
        }

    }

    private static void typeInstructions() {
        System.out.println("Calculates the number of happy tickets depending on algorithm, specified in" +
                " file, given by the fist parameter.");
    }

}
